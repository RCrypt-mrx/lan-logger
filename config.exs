%{
  interface: "wlp3s0",  # NIC to scan
  threads: 10,          # max concurrency factor
  interval: 5_000       # time interval
}
