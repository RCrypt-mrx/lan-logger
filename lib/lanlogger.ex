defmodule Lanlogger do
  @spec scan_all(Path.t()) :: any()
  def scan_all(config_path \\ "config.exs") do
    {config, _e} = Code.eval_file(config_path)
    {:ok, interfaces} = :inet.getifaddrs
    network =
      case Enum.find(interfaces, fn {ifname, _params} -> ifname == config.interface |> String.to_charlist end) do
        {_ifname, params} ->
          {o1, o2, o3, _o4} = params[:addr]
          "#{o1}.#{o2}.#{o3}"
        nil -> IO.puts("Interface name invalid")
      end
    ping = fn count ->
      full_ip = "#{network}.#{count}"
      {_result, status} = System.cmd("ping", ["-c", "1", "-s", "1", full_ip])
      if status == 0, do: full_ip
    end
    max_concurrency = System.schedulers_online() * config.threads # first check only
    Task.async_stream(Enum.to_list(1..254), &ping.(&1), max_concurrency: max_concurrency, timeout: :infinity)
    |> Enum.to_list |> Enum.filter(fn {:ok, e} -> e != nil end)
    |> write(config)
  end

  @spec write(list(), Path.t()) :: any()
  defp write(result, config) do
    {:ok, :online} = :dets.open_file(:online, type: :set)       # for online links
    {:ok, :history} = :dets.open_file(:history, type: :bag)     # for link history
    Enum.each(result, fn {:ok, ipaddr} ->
      case :dets.lookup(:online, ipaddr) do
        [] ->
          :dets.insert_new(:online, {ipaddr, NaiveDateTime.utc_now, 0})
        [{ipaddr, login_time, _seconds_left}] ->
          :dets.insert(:online, {ipaddr, login_time,
            NaiveDateTime.diff(login_time, NaiveDateTime.utc_now, :second)})
      end
    end)
    Enum.each(:dets.match_object(:online, {:_, :_, :_}), fn {ipaddr, login_time, seconds_left} ->
      if !(Enum.find_value(result, fn {ipaddress, _status} ->
        ipaddress == ipaddr
      end)) do
        :dets.delete(:online, ipaddr)
        :dets.insert(:history, {ipaddr, login_time, seconds_left, NaiveDateTime.utc_now})
      end
    end)
    :timer.sleep(config.interval)
    scan_all(config)
  end
end
